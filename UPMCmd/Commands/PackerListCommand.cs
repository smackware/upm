﻿using ManyConsole;
using UPM.Package;

namespace UPMCMD.Commands
{
    class PackerListCommand : ConsoleCommand
    {        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PackerListCommand() : base()
        {
            this.IsCommand("packer-list", "List the local project's packable packages");
        }

        public override int Run(string[] remainingArguments)
        {
            Packer packer = new Packer();
            var packerConfig = packer.LoadPackerConfig();
            foreach (var package in packerConfig.Packages)
            {
                log.Info(package.name + "-" + package.version);
            }
            return 0;
        }
    }
}
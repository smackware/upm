﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;
using UPM.Package;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class RemoveCommand : ConsoleCommand
    {
      //  private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public RemoveCommand()
            : base()
        {
            this.IsCommand("remove", "Remove a package from this project");
            this.AllowsAnyAdditionalArguments("[NAME] ...");        
        }

        public override int Run(string[] remainingArguments)
        {
            var packageDB = new LocalPackageDB();
            packageDB.Remove(remainingArguments[0]);
            return 0;
        }
    }
}

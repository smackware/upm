﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;
using UPM.Package;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class ListCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ListCommand()
            : base()
        {
            this.IsCommand("list", "List installed packages");
        }

        public override int Run(string[] remainingArguments)
        {
            var packageDB = new LocalPackageDB();
            foreach (var packageModel in packageDB.GetInstalledPackages())
            {
                log.Info(packageModel.Name + "-" + packageModel.Version);
            }
            return 0;
        }
    }
}

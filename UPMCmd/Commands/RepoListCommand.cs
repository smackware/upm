﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Repository;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class RepoListCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RepoListCommand()
            : base()
        {
            this.IsCommand("repo-list", "List packages available in remote repositories");
            this.AllowsAnyAdditionalArguments();
        }

        public override int Run(string[] remainingArguments)
        {
            var remoteRepo = new MultipleRemoteRepositoriesDB();
            foreach (var packageModel in remoteRepo.ListPackages())
            {
                log.Info(packageModel.GetFullName());
            }
            return 0;
        }
    }
}

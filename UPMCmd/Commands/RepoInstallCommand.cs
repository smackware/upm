﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Repository;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class RepoInstallCommand : ConsoleCommand
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RepoInstallCommand()
            : base()
        {
            this.IsCommand("repo-install", "Install a package from the remote repository");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
        }

        public override int Run(string[] remainingArguments)
        {
            var remoteRepo = new MultipleRemoteRepositoriesDB();
            foreach (var packageName in remainingArguments)
            {
                remoteRepo.Install(packageName, null);
            }
            return 0;
        }
    }
}

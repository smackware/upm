﻿using ManyConsole;
using UPM.Package;

namespace UPMCMD.Commands
{
    class PackAllCommand : ConsoleCommand
    {
        private const string PACKAGE_PACKER_CONFIG_FILENAME = "package_info.json";
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PackAllCommand() : base()
        {
            this.IsCommand("pack-all", "Archive all the local project's packages for distribution");
        }

        public override int Run(string[] remainingArguments)
        {
            Packer packer = new Packer();
            packer.PackAll();
            return 0;
        }
    }
}

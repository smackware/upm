﻿using ManyConsole;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;
using UPM.Utility;

namespace UPM.Commands
{
    class InitCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string rootFolder = null;
        private string folder = null;

        public InitCommand()
            : base()
        {
            this.IsCommand("init", "Initializes a blank packages.json in the assets root");

            this.HasOption("rpd|rootPackageFolder=", "Sets the root folder for installed packages", v => this.rootFolder = v);
            this.HasOption("if|inFolder:", "place in specific folder(or current if not given a folder)", v => this.folder = v ?? ".");
        }

        public override int Run(string[] remainingArguments)
        {
            // find assets root folder
            DirectoryInfo assetsDir;
            if (folder != null)
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), folder);
                if (Directory.Exists(path))
                    assetsDir = new DirectoryInfo(path);
                else
                {
                    log.Error("Folder "+ path +" does not exist");
                    return -1;
                }
            }
            else
            {
                assetsDir = PathUtility.GetAssetsDirectory();
            }

            if (assetsDir != null)
            {
                var config = ConfigUtility.LoadConfig();

                // build packages.json model
                var model = new PackageModel();
                model.RootPackageFolder = rootFolder ?? config.RootPackageDir;
                
                // write packages.json
                log.Info("Writing base package.json to " + assetsDir.FullName);
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                var json = JsonConvert.SerializeObject(model, Formatting.Indented, settings);
                File.WriteAllText(Path.Combine(assetsDir.FullName, "package.json"), json, Encoding.UTF8);
            }
            else
            {
                log.Error("Assets folder not found");
                return -1;
            }

            return 0;
        }
    }
}

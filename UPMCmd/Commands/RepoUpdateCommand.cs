﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Package;

namespace UPMCMD.Commands
{
    class RepoUpdateCommand : ConsoleCommand
    {
        public RepoUpdateCommand()
            : base()
        {
            this.IsCommand("repo-update", "Scan the local directory for packages and update the repository information");
            this.SkipsCommandSummaryBeforeRunning();
        }

        public override int Run(string[] remainingArguments)
        {
            var repo = new LocalRepositoryDB();
            repo.UpdateRepository();
            return 0;
        }
    }
}

﻿using ManyConsole;
//using System.Threading.Tasks;
using UPM.Package;

namespace UPMCMD.Commands
{
    class ValidateCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private bool shouldListContents;

        public ValidateCommand()
            : base()
        {
            this.IsCommand("validate", "Validate the contents of an installed package");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
        }

        public override int Run(string[] remainingArguments)
        {
            var packageDB = new LocalPackageDB();
            var errors = packageDB.ValidateInstalledPackage(remainingArguments[0]);
            foreach (var error in errors)
            {
                log.Warn(error);
            }
            return errors.Length == 0 ? 0 : 1;
        }
    }
}

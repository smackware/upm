﻿using ManyConsole;
//using System.Threading.Tasks;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class InspectCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private bool shouldListContents;

        public InspectCommand()
            : base()
        {
            this.IsCommand("inspect", "Inspects a package archive and prints information");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
            this.HasOption("f|full", "Also list contents of the archive", v => this.shouldListContents = true);
        }

        public override int Run(string[] remainingArguments)
        {
            var archiveFilePath = remainingArguments[0];
            var packageModel = PackageArchiveUtility.GetPackageModelFromArchive(archiveFilePath);
            if (packageModel == null)
            {
                log.Error("Couldn't read package from archive...");
                return -1;
            }
            log.Info("Name: " + packageModel.Name);
            log.Info("Version: " + packageModel.Version);
            foreach (var dep in packageModel.Dependencies)
            {
                log.Info("Depends on: " + dep.Name + "-" + dep.MinimumVersion);
            }
            if (shouldListContents)
            {
                foreach (var packageFile in packageModel.Files)
                {
                    log.Info("Contains file: " + packageFile.Path + " (" + packageFile.Checksum + ")");
                }
            }
            return 0;
        }
    }
}

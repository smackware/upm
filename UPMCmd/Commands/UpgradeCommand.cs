﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using UPM.Model;
using UPM.Package;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class UpgradeCommand : ConsoleCommand
    {        
        public UpgradeCommand() : base()
        {
            this.IsCommand("upgrade", "Upgrade a package from a package file");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
        }

        public override int Run(string[] remainingArguments)
        {
            var packageDB = new LocalPackageDB();
            packageDB.Upgrade(remainingArguments[0]);
            return 0;
        }
    }
}

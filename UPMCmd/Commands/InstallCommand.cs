﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;
using UPM.Package;
using UPM.Utility;

namespace UPMCMD.Commands
{
    class InstallCommand : ConsoleCommand
    {
        public InstallCommand()
            : base()
        {
            this.IsCommand("install", "Install a package from a package file");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
        }

        public override int Run(string[] remainingArguments)
        {
            var packageDB = new LocalPackageDB();
            packageDB.Install(remainingArguments[0]);
            return 0;
        }
    }
}

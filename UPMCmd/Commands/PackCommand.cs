﻿using ManyConsole;
using UPM.Package;

namespace UPMCMD.Commands
{
    class PackCommand : ConsoleCommand
    {
        private const string PACKAGE_PACKER_CONFIG_FILENAME = "package_info.json";
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PackCommand() : base()
        {
            this.IsCommand("pack", "Archive one of the local project's packages for distribution");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
        }

        public override int Run(string[] remainingArguments)
        {
            Packer packer = new Packer();
            packer.Pack(remainingArguments[0]);
            return 0;
        }
    }
}

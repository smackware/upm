﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UPM.Model
{
    public class PackageDependency
    {        
        public string Name { get; set; }
        public string MinimumVersion { get; set; }

        public override string ToString()
        {
            return string.Format("[PackageDependency: Name={0}, MinimumVersion={1}]", Name, MinimumVersion);
        }
    }
}

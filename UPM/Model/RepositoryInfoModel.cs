﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UPM.Model
{
    class RepositoryInfoModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public int Priority { get; set; }
    }
}

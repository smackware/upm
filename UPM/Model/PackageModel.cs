﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UPM.Model
{
    public class PackageModel
    {        
        public string Name { get; set; }
        public string Version { get; set; }
        public string Author { get; set; }
        public string[] Authors { get; set; }
        public string Url { get; set; }
        public PackageDependency[] Dependencies { get; set; }
        public string RootPackageFolder { get; set; }
        public PackageFileList Files { get; set;}

        public PackageModel()
        {
            Dependencies = null;
            RootPackageFolder = null;
            Name = "Unity Project";
            Version = "0.0.0";
        }

        public override string ToString()
        {
            return string.Format("[PackageModel: Name={0}, Version={1}]", Name, Version);
        }            

        public string GetFullName()
        {
            return string.Format("{0}-{1}", Name, Version);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UPM.Model
{
    public class PackagePackingModel
    {
        public string name { get; set; }
        public string url { get; set; }
        public string version { get; set; }
        public string[] filePathMasks { get; set; }
        public PackageDependency[] dependencies { get; set; }
    }
}


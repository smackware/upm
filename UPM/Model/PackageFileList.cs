﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UPM.Model
{
    public class PackageFileList : List<PackageFile>
    {          

        public PackageFileList() : base()
        {
        }
        

        public PackageFileList(IEnumerable<PackageFile> collection) : base(collection)
        {            
        }
        
        public string[] GetFilePaths()
        {
            var filePaths = new string[Count];
            for (int i = 0; i < Count; i++)
            {
                filePaths[i] = this[i].Path;
            }
            return filePaths;
        }
    }
}

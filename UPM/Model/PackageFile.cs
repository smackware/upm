﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UPM.Model
{
    public class PackageFile
    {        
        public string Path { get; set; }
        public string Checksum { get; set; }

        public PackageFile()
        {
        }        

        public override string ToString()
        {
            return Path;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO.Compression; // Careful - zipstorer is not a real C# built-in
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;

namespace UPM.Utility
{
    public static class PackageArchiveUtility
    {
        public const string PACKAGE_ARCHIVE_FILENAME_TEMPLATE = "{0}-{1}.zip";

        public const string PACKAGE_MODEL_FILENAME = "package.json";


        public static string GetPackageFileName(string name, string version)
        {
            return string.Format(PACKAGE_ARCHIVE_FILENAME_TEMPLATE, name, version);
        }


        public static string GetPackageFileName(PackageModel model)
        {
            return GetPackageFileName(model.Name, model.Version);
        }


        public static PackageModel GetPackageModelFromArchive(string pathToArchive)
        {
            if (!File.Exists(pathToArchive))
            {
                throw new Exception("File not found");
            }
                
            MemoryStream jsonContentStream = new MemoryStream();
            var zipFile = ZipStorer.Open(pathToArchive, FileAccess.Read);
            List<ZipStorer.ZipFileEntry> zipContents = zipFile.ReadCentralDir();
            foreach (var zipEntry in zipContents)
            {
                
                if (zipEntry.FilenameInZip == PACKAGE_MODEL_FILENAME)
                {
                    if (!zipFile.ExtractFile(zipEntry, jsonContentStream))
                    {
                        throw new Exception("Failed to extract file: " + PACKAGE_MODEL_FILENAME);
                    }
                    break;
                }
            }
            zipFile.Close();
            jsonContentStream.Flush();
            jsonContentStream.Position = 0;
            var jsonData = new StreamReader(jsonContentStream).ReadToEnd();
            return JsonUtility.LoadString<PackageModel>(jsonData);
        }


        public static void CreatePackageArchive(PackageModel packageModel)
        {
            var tmpDir = PathUtility.GetTempDirectory();
            var temporaryPackageModelFilePath = PathUtility.PathCombine(tmpDir, PACKAGE_MODEL_FILENAME);
            JsonUtility.SaveFile(packageModel, temporaryPackageModelFilePath);
            var packageFileName = GetPackageFileName(packageModel);
            var zipFile = ZipStorer.Create(packageFileName, "");
            //var p = System.Diagnostics.Process.Start("zip", packageFileName + " package.json");
            //p.WaitForExit();
            Console.WriteLine("Packing metadata");
            zipFile.AddFile(ZipStorer.Compression.Deflate, temporaryPackageModelFilePath, PACKAGE_MODEL_FILENAME, "Metadata");
            foreach (var filePath in packageModel.Files.GetFilePaths())
            {
                Console.WriteLine("Packing " + filePath);
                zipFile.AddFile(ZipStorer.Compression.Deflate, filePath, filePath, "");
            }
            zipFile.Close();
            File.Delete(temporaryPackageModelFilePath);
        }


        public static PackageModel ExtractPackageArchive(string pathToArchive, string extractionPath)
        {
            var zipFile = ZipStorer.Open(pathToArchive, FileAccess.Read);
            List<ZipStorer.ZipFileEntry> zipContents = zipFile.ReadCentralDir();
            PackageModel packageModel = null;
            foreach (var zipEntry in zipContents)
            {
                var extractedFilePath = Path.Combine(extractionPath, zipEntry.FilenameInZip);
                if (!zipFile.ExtractFile(zipEntry, extractedFilePath))
                {
                    zipFile.Close();
                    throw new Exception("Failed to extract file: " + zipEntry.FilenameInZip);
                }
                if (zipEntry.FilenameInZip == PACKAGE_MODEL_FILENAME)
                {
                    packageModel = JsonUtility.LoadFile<PackageModel>(extractedFilePath);
                }
            }
            zipFile.Close();
            if (packageModel == null)
            {
                throw new Exception("Failed to load package model");
            }
            return packageModel;
        }
            

    }
}

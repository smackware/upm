﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;

namespace UPM.Utility
{
    internal static class VersionUtility
    {
        /// <summary>
        /// Compares the versions.
        /// </summary>
        /// <returns>1 if versionA is greater, -1 if versionA is lesser, 0 if versions are equal</returns>
        /// <param name="versionA">Version a.</param>
        /// <param name="versionB">Version b.</param>
        public static int CompareVersions(string versionA, string versionB)
        {
            var verBIntSegments = ParseVersionString(versionB);
            var verAIntSegments = ParseVersionString(versionA);

            for (int i = 0 ; i < verBIntSegments.Length ; i++)
            {
                // If I'm 9.0 and I get compared to 9.0.1, I am considered "older"
                if (verAIntSegments.Length <= i)
                {
                    if (verBIntSegments[i] == 0)
                    {
                        continue;
                    }
                    return -1;
                }
                if (verAIntSegments[i] > verBIntSegments[i])
                {
                    return 1;
                }
                if (verAIntSegments[i] < verBIntSegments[i])
                {
                    return -1;
                }
            }
            // If I'm 9.0.1 and I get compared to 9.0, I am considered "newer"
            // If I'm 9.0.0 and I get compared to 9.0 I am equal
            for (int i = verBIntSegments.Length; i < verAIntSegments.Length ; i++)
            {
                if (verAIntSegments[i] != 0)
                {
                    return 1;
                }
            }
            // Exact match
            return 0;
        }

        private static int[] ParseVersionString(string versionString)
        {            
            string[] versionStringSegments = versionString.Split('.');
            int[] versionIntSegments = new int[versionStringSegments.Length];
            for (int i = 0; i < versionStringSegments.Length; i++)
            {
                var stringSegment = versionStringSegments[i];
                int intSegment;
                if (!int.TryParse(stringSegment, out intSegment))
                {
                    intSegment = 0;
                }
                versionIntSegments[i] = intSegment;
            }
            return versionIntSegments;
        }
    }
}

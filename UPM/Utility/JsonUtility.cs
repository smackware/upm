﻿using System;
using System.IO;
using LitJson;

namespace UPM.Utility
{
    public static class JsonUtility
    {
        public static T LoadFile<T>(string jsonFilePath)
        {

            if (jsonFilePath == null)
            {
                throw new Exception("File not found: " + jsonFilePath);
            }
                
            if (!File.Exists(jsonFilePath))
            {
                return default(T);
            }

            // load package.json in current directory
            var json = File.ReadAllText(jsonFilePath);
            return LoadString<T>(json);
        }

        public static T LoadString<T>(string jsonData)
        {
            return JsonMapper.ToObject<T>(jsonData);
        }

        public static void SaveFile<T> (T data, string jsonFilePath)
        {
            var jsonContent = JsonMapper.ToJson(data);
            File.WriteAllText(jsonFilePath, jsonContent);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
//using System.Threading.Tasks;

namespace UPM.Utility
{
    static class PathUtility
    {
        public static DirectoryInfo GetAssetsDirectory()
        {
            var currentPath = new DirectoryInfo(Directory.GetCurrentDirectory());
            var assetsDirectories = currentPath.GetDirectories("Assets", SearchOption.TopDirectoryOnly);

            while (assetsDirectories.Length == 0 && currentPath != null)
            {
                // not in current dir, look up
                currentPath = currentPath.Parent;
                if (currentPath!=null)
                    assetsDirectories = currentPath.GetDirectories("Assets", SearchOption.TopDirectoryOnly);
            }

            if (assetsDirectories.Length > 0)
                return assetsDirectories[0];
            else
                return null;
        }


        public static string GetTempDirectory()
        {
            return Path.GetTempPath();
        }

        public static string GetConfigFilePath()
        {
            var path = PathUtility.PathCombine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".UPM", "config.json");
            return path;
        }

        public static string CalculateChecksum(string filePath)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filePath))
                {
                    return Convert.ToBase64String(md5.ComputeHash(stream));
                }
            }
        }


        public static List<string> GlobFileMask(string fileMasks)
        {            
            var includedFilePaths = new List<string>();
            if (!Directory.Exists(Directory.GetCurrentDirectory()))
            {
                throw new SystemException("Current directory does not exist!");
            }

            foreach (var filePathToPack in Glob(Path.Combine(".", fileMasks)))
            {                
                includedFilePaths.Add(filePathToPack);
            }
            return includedFilePaths;
        }
            
        public static string PathCombine(params string[] subPaths)
        {
            var path = subPaths[0];
            for (int i = 1 ; i <subPaths.Length; i++)
            {
                path = Path.Combine(path, subPaths[i]);
            }
            return path;
        }

        public static IEnumerable<string> RecursePath(string parent)
        {            
            if (!Directory.Exists(parent))
            {
                yield return parent;
                yield break;
            }
            foreach (var subPath in Directory.GetDirectories(parent))
            {
                foreach (var result in RecursePath(subPath))
                {                    
                    yield return result;
                }
            }
            foreach (var subPath in Directory.GetFiles(parent))
            {                
                yield return subPath;
            }
        }

        public static IEnumerable<string> RecursePaths(string[] parents)
        {
            foreach (var parent in parents)
            {
                foreach (var result in RecursePath(parent))
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// return a list of files that matches some wildcard pattern, e.g. 
        /// C:\p4\software\dotnet\tools\*\*.sln to get all tool solution files
        /// </summary>
        /// <param name="glob">pattern to match</param>
        /// <returns>all matching paths</returns>
        public static IEnumerable<string> Glob(string glob)
        {
            foreach (string path in Glob(PathHead(glob) + DirSep, PathTail(glob)))
                yield return path;
        }

        /// <summary>
        /// uses 'head' and 'tail' -- 'head' has already been pattern-expanded
        /// and 'tail' has not.
        /// </summary>
        /// <param name="head">wildcard-expanded</param>
        /// <param name="tail">not yet wildcard-expanded</param>
        /// <returns></returns>
        public static IEnumerable<string> Glob(string head, string tail)
        {
            if (PathTail(tail) == tail)
            {
                foreach (string path in Directory.GetFiles(head, tail).OrderBy(s => s))
                {
                    yield return path;
                }
            }
            else
            {
                foreach (string dir in Directory.GetDirectories(head, PathHead(tail)).OrderBy(s => s))
                {
                    foreach (string path in Glob(Path.Combine(head, dir), PathTail(tail)))
                    {
                        yield return path;
                    }                        
                }                
            }   
        }

        /// <summary>
        /// shortcut
        /// </summary>
        static char DirSep = Path.DirectorySeparatorChar;

        /// <summary>
        /// return the first element of a file path
        /// </summary>
        /// <param name="path">file path</param>
        /// <returns>first logical unit</returns>
        static string PathHead(string path)
        {
            // handle case of \\share\vol\foo\bar -- return \\share\vol as 'head'
            // because the dir stuff won't let you interrogate a server for its share list
            // FIXME check behavior on Linux to see if this blows up -- I don't think so
            if (path.StartsWith("" + DirSep + DirSep))
                return path.Substring(0, 2) + path.Substring(2).Split(DirSep)[0] + DirSep + path.Substring(2).Split(DirSep)[1];

            return path.Split(DirSep)[0];
        }

        /// <summary>
        /// return everything but the first element of a file path
        /// e.g. PathTail("C:\TEMP\foo.txt") = "TEMP\foo.txt"
        /// </summary>
        /// <param name="path">file path</param>
        /// <returns>all but the first logical unit</returns>
        static string PathTail(string path)
        {
            if (!path.Contains(DirSep))
            {
                return path;
            }
             
            return path.Substring(1 + PathHead(path).Length);
        }
    }
}

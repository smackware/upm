﻿using System;
using System.IO;
using UPM.Utility;
using UPM.Model;

namespace UPM.Repository
{
    public class MultipleRepositoryConfigManager
    {
        public const string DEFAULT_CONFIG_FILENAME = "upmRemoteRepository.json";

        public static bool IsDefaultConfigAvailable()
        {
            return File.Exists(DEFAULT_CONFIG_FILENAME);
        }

        public static RemoteRepositoryConfigModel LoadConfigFile(string configFilePath)
        {
            var remoteRepoConfigModel = JsonUtility.LoadFile<RemoteRepositoryConfigModel>(configFilePath);
            if (remoteRepoConfigModel == null)
            {
                throw new Exception("Cannot parse configuration file " + configFilePath);
            }
            return remoteRepoConfigModel;
        }

        public static void SaveConfigFile(string configFilePath, RemoteRepositoryConfigModel config)
        {
            JsonUtility.SaveFile(config, configFilePath);
        }


        public static RemoteRepositoryConfigModel FromDefaultConfigFile()
        {
            return LoadConfigFile(DEFAULT_CONFIG_FILENAME);
        }            
    }
}


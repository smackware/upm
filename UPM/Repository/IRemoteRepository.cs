﻿using System;
using UPM.Model;

namespace UPM.Repository
{
    public interface IRemoteRepository
    {

        PackageModel[] ListPackages();

        PackageModel FindPackage(string name, string version);  

        PackageModel FindMinimumPackage(string name, string minimumVersion);
    
        void Install(string name, string version);
    }
}


﻿using System;
using System.IO;
using System.Collections.Generic;
using UPM.Model;
using UPM.Utility;

namespace UPM.Repository
{
    public class MultipleRemoteRepositoriesDB : IRemoteRepository
    {


        private readonly RemoteRepositoryDB[] _remoteRepositories;


        public MultipleRemoteRepositoriesDB() : this(MultipleRepositoryConfigManager.FromDefaultConfigFile())
        {
            
        }
       
        public MultipleRemoteRepositoriesDB(RemoteRepositoryConfigModel remoteRepoConfigModel)
        {
            var repoCount = remoteRepoConfigModel.URLs.Length;
            _remoteRepositories = new RemoteRepositoryDB[repoCount];
            for (int i = 0; i < repoCount; i++)
            {
                var repoUrl = remoteRepoConfigModel.URLs[i];
                var repo = new RemoteRepositoryDB(repoUrl);
                _remoteRepositories[i] = repo;
            }
        }            

        public PackageModel[] ListPackages()
        {
            Dictionary<string, PackageModel> remotePackagesByNameAndVersion = new Dictionary<string, PackageModel>();
            foreach (var subRepo in _remoteRepositories)
            {
                foreach (var packageModel in subRepo.ListPackages())
                {
                    var packageKey = packageModel.Name + '-' + packageModel.Version;
                    if (!remotePackagesByNameAndVersion.ContainsKey(packageKey))
                    {
                        remotePackagesByNameAndVersion[packageKey] = packageModel;
                    }
                }
            }
            return new List<PackageModel>(remotePackagesByNameAndVersion.Values).ToArray();
        }

        public UPM.Model.PackageModel FindPackage(string name, string version)
        {
            foreach (var subRepo in _remoteRepositories)
            {
                var found = subRepo.FindPackage(name, version);
                if (found != null)
                {
                    return found;
                }
            }      
            return null;
        }
            

        public UPM.Model.PackageModel FindMinimumPackage(string name, string minimumVersion)
        {
            foreach (var subRepo in _remoteRepositories)
            {
                var found = subRepo.FindMinimumPackage(name, minimumVersion);
                if (found != null)
                {
                    return found;
                }
            }
            return null;
        }

        public void Install(string name, string version)
        {
            var repo = FindRepoContaining(name, version);
            if (repo == null)
            {
                throw new Exception("Package not available in any remote repository");
            }
            repo.Install(name, version);
        }

        private IRemoteRepository FindRepoContaining(string name, string version)
        {
            foreach (var subRepo in _remoteRepositories)
            {
                var found = subRepo.FindPackage(name, version);
                if (found != null)
                {
                    return subRepo;
                }
            }      
            return null;
        }
    }
}


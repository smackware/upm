﻿using System;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using UPM.Utility;
using UPM.Model;
using UPM.Package;

namespace UPM.Repository
{
    public class RemoteRepositoryDB : IRemoteRepository
    {
        
        private WebClient webClient = null;
        private RemoteRepositoryModel repositoryModel;

        public RemoteRepositoryDB(string baseUrl)
        {
            webClient = new WebClient();
            webClient.BaseAddress = baseUrl;
        }            

        public void CacheRepository()
        {
            //log.Info("Caching package information from remote repository");
            var repoJsonData = webClient.DownloadString("upmRepository.json");
            repositoryModel = JsonUtility.LoadString<RemoteRepositoryModel>(repoJsonData);
            //log.Info("Cached data for " + repositoryModel.Packages.Length + " packages");
        }

        public PackageModel[] ListPackages()
        {
            var repoModel = GetRepositoryModel();
            return repoModel.Packages;
        }

        public PackageModel FindPackage(string name, string version)
        {
            var repoModel = GetRepositoryModel();
            var lowerName = name.ToLower();
            var lowerVersion = version != null ? version.ToLower() : null;
            for (int i = 0 ; i < repoModel.Packages.Length ; i++)
            {
                var packageModel = repoModel.Packages[i];
                if (packageModel.Name.ToLower() == lowerName)
                {
                    if (string.IsNullOrEmpty(lowerVersion) || packageModel.Version.ToLower() == lowerVersion)
                    {
                        return packageModel;
                    }
                }
            }
            return null;
        }            

        public PackageModel FindMinimumPackage(string name, string minimumVersion)
        {
            var repoModel = GetRepositoryModel();
            var lowerName = name.ToLower();
            //var lowerVersion = minimumVersion;
            for (int i = 0 ; i < repoModel.Packages.Length ; i++)
            {
                var packageModel = repoModel.Packages[i];
                if (packageModel.Name.ToLower() == lowerName)
                {
                    if (VersionUtility.CompareVersions(minimumVersion, packageModel.Version) <= 0)
                    {
                        return packageModel;
                    }
                }
            }
            return null;
        }       
            
        public void Install(string name, string version)
        {           
            var localPackageDB = new LocalPackageDB();
            InstallWithDependencies(name, version, localPackageDB);
        }
            
        private void InstallWithDependencies(string name, string version, LocalPackageDB localPackageDB)
        {               
            var packageModel = FindPackage(name, version);
            if (packageModel == null)
            {
                throw new Exception("Cannot get package information from archive");
            }                
            var missingDeps = localPackageDB.GetMissingDependencies(packageModel);
            foreach (var missingDep in missingDeps)
            {
                var depPackageModel = FindMinimumPackage(missingDep.Name, missingDep.MinimumVersion);
                InstallWithDependencies(depPackageModel.Name, depPackageModel.Version, localPackageDB);
            }
            TemporarilyDownload(name, version, localPackageDB.InstallOrUpgrade);
        }

        private PackageDependency[] GetUnsatisfiedDependencies(PackageModel package, LocalPackageDB localPackageDB)
        {
            Dictionary<string, PackageDependency> required = new Dictionary<string, PackageDependency>();
            Queue<PackageModel> packagesToExpand = new Queue<PackageModel>();
            packagesToExpand.Enqueue(package);

            while (packagesToExpand.Count > 0)
            {
                var currentPackage = packagesToExpand.Dequeue();
                foreach (var dep in localPackageDB.GetMissingDependencies(currentPackage))
                {
                    PackageDependency requiredDependency;
                    if (required.TryGetValue(dep.Name, out requiredDependency))
                    {
                        if (VersionUtility.CompareVersions(requiredDependency.MinimumVersion, dep.MinimumVersion) > 0)
                        {
                            // Dependency already noted
                            continue;
                        }
                    }
                    var foundPackage = FindPackage(dep.Name, null);
                    if (foundPackage == null)
                    {
                        throw new Exception("Cannot satisfy dependencies");
                    }
                    if (VersionUtility.CompareVersions(foundPackage.Version, dep.MinimumVersion) > 0)
                    {
                        throw new Exception("Cannot satisfy dependencies");
                    }
                    required[foundPackage.Name] = new PackageDependency 
                    { 
                        Name = foundPackage.Name, 
                        MinimumVersion = foundPackage.Version 
                    };
                    packagesToExpand.Enqueue(foundPackage);
                }
            }
            return new List<PackageDependency>(required.Values).ToArray();
        }            

        private void TemporarilyDownload(string name, string version, Action<string> onDownload)
        {            
            var packageModel = FindPackage(name, version);
            var tempDir = PathUtility.GetTempDirectory();
            Directory.CreateDirectory(tempDir);

            var packageFileName = PackageArchiveUtility.GetPackageFileName(packageModel.Name, packageModel.Version);
            var downloadedFilePath = Path.Combine(tempDir, packageFileName);
            webClient.DownloadFile(packageFileName, downloadedFilePath);
            if (onDownload != null)
            {
                onDownload(downloadedFilePath);
            }    
            File.Delete(downloadedFilePath);
        }

        private RemoteRepositoryModel GetRepositoryModel()
        {
            if (repositoryModel == null)
            {
                CacheRepository();
            }
            return repositoryModel;
        }
            
    }
}
    
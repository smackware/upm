﻿using System;

namespace UPM.Package.Exceptions
{
    public class InstallationFailedException : OperationCanceledException
    {
        public InstallationFailedException(string reason) : base(reason)
        {
        }
        
    }
}


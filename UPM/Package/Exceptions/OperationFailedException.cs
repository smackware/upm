﻿using System;

namespace UPM.Package.Exceptions
{
    public class OperationFailedException : Exception
    {
        public readonly string Reason;

        public OperationFailedException(string reason)
        {
            Reason = reason;
        }
    }
}


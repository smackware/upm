﻿using System;
using System.IO;
using System.Collections.Generic;
using UPM.Utility;
using UPM.Model;

namespace UPM.Package
{
    public class Packer
    {
        private const string PACKAGE_PACKER_CONFIG_FILENAME = "packages_info.json";


        public PackagePackerConfigModel LoadPackerConfig()
        {
            return JsonUtility.LoadFile<PackagePackerConfigModel>(PACKAGE_PACKER_CONFIG_FILENAME);
        }

        public int Pack(string name)
        {
            var lowerName = name.ToLower();
            var packerConfig = LoadPackerConfig();
            if (packerConfig == null)
            {
                throw new Exception("Couldn't load packer configuration");
            }
            ValidateFilesAreNotSharedBetweenPackages(packerConfig);
            foreach (var packagePackingModel in packerConfig.Packages)
            {                
                if (packagePackingModel.name.ToLower() == lowerName)
                {
                    var packageModel = CreatePackageModel(packagePackingModel);
                    PackageArchiveUtility.CreatePackageArchive(packageModel);
                    return 0;
                }
            }
            throw new Exception("Couldn't pack package - name is not in the packer config file");
        }

        public int PackAll()
        {            
            var packerConfig = LoadPackerConfig();
            if (packerConfig == null)
            {
                throw new Exception("Couldn't load packer configuration");
            }
            ValidateFilesAreNotSharedBetweenPackages(packerConfig);
            foreach (var packagePackingModel in packerConfig.Packages)
            {
                var packageModel = CreatePackageModel(packagePackingModel);
                PackageArchiveUtility.CreatePackageArchive(packageModel);
            }
            return 0;
        }

        private void ValidateFilesAreNotSharedBetweenPackages(PackagePackerConfigModel packerConfig)
        {
            var sharedFile = FindSharedFilePath(packerConfig.Packages);
            if (sharedFile != null)
            {
                throw new Exception("Cannot pack: The file " + sharedFile + " is shared among multiple packages.");
            }
        }

        private string FindSharedFilePath(PackagePackingModel[] packerModels)
        {
            HashSet<string> scannedFiles = new HashSet<string>();
            foreach (var packerModel in packerModels)
            {
                var scannedFilePaths = PathUtility.RecursePaths(packerModel.filePathMasks);
                foreach (var filePath in scannedFilePaths)
                {
                    if (scannedFiles.Contains(filePath))
                    {
                        return filePath;
                    }
                    scannedFiles.Add(filePath);
                }
            }
            return null;
        }

        /*
        private PackageDependency[] GenerateDependencyList(PackageModel[] packageModels)
        {
            PackageDependency[] deps = new PackageDependency[packageModels.Length];
            for (int i=0; i < packageModels.Length; i++)
            {
                var sourcePackageModel = packageModels[i];
                deps[i] = new PackageDependency
                {                        
                        Name = sourcePackageModel.Name,
                        MinimumVersion = sourcePackageModel.Version,
                };
            }
            return deps;
        }*/

        private string[] FilterFilesOfOtherPackages(IEnumerable<string> rawFileList, LocalPackageDB localPackageDB)
        {
            var otherPackageFiles = new HashSet<string>();
            var installedPackages = localPackageDB.GetInstalledPackages();
            var filteredFileList = new List<string>();
            foreach (var packageModel in installedPackages)
            {
                otherPackageFiles.UnionWith(packageModel.Files.GetFilePaths());
            }

            foreach (var filepath in rawFileList)
            {
                if (!otherPackageFiles.Contains(filepath))
                {
                    filteredFileList.Add(filepath);
                }
            }
            return filteredFileList.ToArray();
        }



        private PackageFileList GenerateChecksumedPackageFileList(string[] pathsToInclude)
        {
            var fileCount = pathsToInclude.Length;
            var packageFiles = new PackageFile[fileCount];
            for (int i = 0; i < fileCount; i++)
            {
                var filePath = pathsToInclude[i];
                var checksum = PathUtility.CalculateChecksum(filePath);
                var packageFile = new PackageFile()
                {
                        Path = filePath,
                        Checksum = checksum,
                };
                packageFiles[i] = packageFile;
            }

            return new PackageFileList(packageFiles);

        }

        private PackageModel CreatePackageModel(PackagePackingModel packerModel)
        {
            var localPackageDB = new LocalPackageDB();
            var packageDeps = packerModel.dependencies; // GenerateDependencyList(installedPackages);
            var packageModel = new PackageModel();
            var scannedFilePaths = PathUtility.RecursePaths(packerModel.filePathMasks);
            var includedFilePaths = FilterFilesOfOtherPackages(scannedFilePaths, localPackageDB);
            var packageFileList = GenerateChecksumedPackageFileList(includedFilePaths);
            packageModel.Name = packerModel.name;
            packageModel.Version = packerModel.version;
            packageModel.Files = packageFileList;
            packageModel.Dependencies = packageDeps;
            return packageModel;
        }
    }
}


﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using UPM.Model;
using UPM.Utility;
using UPM.Package.Exceptions;

namespace UPM.Package
{
    public class LocalPackageDB
    {

        public readonly string PackageDBDirName = Path.Combine(".upmPackageDB", "installedPackages");


        public LocalPackageDB()
        {
            
        }


        public void Install(string zipFilePath)
        {            
            var conflictingFilePaths = GetPreInstallationConflictingFiles(zipFilePath);
            if (conflictingFilePaths.Length > 0)
            {
                var message = "Conflicting files exist: ";
                foreach (var filePath in conflictingFilePaths)
                {
                    message += "[" + filePath + "]    ";
                }
                throw new InstallationFailedException(message);
            }
            TemporarilyUnpack(zipFilePath, InstallExtractedPackageArchive);
        }


        public void Upgrade(string zipFilePath)
        {
            TemporarilyUnpack(zipFilePath, UpgradeExtractedPackageArchive);
        }


        public void InstallOrUpgrade(string zipFilePath)
        {            
            var newPackageModel = PackageArchiveUtility.GetPackageModelFromArchive(zipFilePath);

            var existingPackageModel = GetInstalledPackageModel(newPackageModel.Name);
            if (existingPackageModel != null)
            {
                Upgrade(zipFilePath);
            }
            else
            {
                Install(zipFilePath);
            }
        }


        public string[] ValidateInstalledPackage(string packageName)
        {            
            var packageModel = GetInstalledPackageModel(packageName);
            if (packageModel == null)
            {
                throw new Exception("Package is not installed");
            }
            return ValidatePackage(null, packageModel);
        }


        public PackageDependency[] GetMissingDependencies(PackageModel packageModel)
        {
            List<PackageDependency> missing = new List<PackageDependency>();
            var deps = packageModel.Dependencies;
            for (int i=0; i < deps.Length; i++)
            {
                var packageDep = deps[i];
                var installedPackageDep = GetInstalledPackageModel(packageDep.Name);
                if (installedPackageDep == null || VersionUtility.CompareVersions(packageDep.MinimumVersion, installedPackageDep.Version) > 0)
                {
                    missing.Add(packageDep);
                }
            }
            return missing.ToArray();
        }


        private PackageModel[] GetDependantPackages(PackageModel packageModel)
        {
            List<PackageModel> dependantPackages = new List<PackageModel>();

            var dependencyName = packageModel.Name.ToLower();
            var installedPackages = GetInstalledPackages();
            for (int i=0; i < installedPackages.Length; i++)
            {
                var tmpPackageModel = installedPackages[i];
                var tmpPackageModelDeps = tmpPackageModel.Dependencies;
                for (int j=0; j < tmpPackageModelDeps.Length; j++)
                {
                    var tmpPackageDep = tmpPackageModelDeps[j];
                    if (tmpPackageDep.Name.ToLower() == dependencyName)
                    {
                        dependantPackages.Add(tmpPackageModel);
                    }
                }
            }

            return dependantPackages.ToArray();
        }


        public void Remove(string packageName)
        {
            var packageModel = GetInstalledPackageModel(packageName);
            if (packageModel == null)
            {
                throw new Exception("Can't remove uninstalled pacakge " + packageName);
            }

            var dependantPackages = GetDependantPackages(packageModel);

            if (dependantPackages.Length > 0)
            {
                var error = "Cannot remove package, there are packages depending on it:";
                foreach (var dependant in dependantPackages)
                {
                    error += " " + dependant;
                }
                throw new Exception(error);
            }

            RemoveUnsafe(packageModel);
        }


        public PackageModel[] GetInstalledPackages()
        {
            if (!Directory.Exists(PackageDBDirName))
            {
                return new PackageModel[0];
            }
            List<PackageModel> installed = new List<PackageModel>();
            foreach (var dirPath in Directory.GetDirectories(PackageDBDirName))
            {                        
                
                var packageName = Path.GetFileName(dirPath);
                var packageModel = GetInstalledPackageModel(packageName);

                installed.Add(packageModel);
            }
            return installed.ToArray();
        }
         

        private void RemoveUnsafe(PackageModel packageModel)
        {
            for (int i = 0 ; i < packageModel.Files.Count ; i++)
            {
                RemovePackageFile(packageModel.Files[i]);
            }
            Directory.Delete(Path.Combine(PackageDBDirName, packageModel.Name), true);   
        }


        private void RemovePackageFile(PackageFile packageFile)
        {            
            var containingDir = Path.GetDirectoryName(packageFile.Path);

            if (File.Exists(packageFile.Path))
            {
                File.Delete(packageFile.Path);
            }

            if (Directory.Exists(containingDir) && Directory.GetFileSystemEntries(containingDir).Length == 0)
            {
                Directory.Delete(containingDir);
            }
        }


        private void RegisterPackageJson(string packageJsonFilepath)
        {
            var packageModel = JsonUtility.LoadFile<PackageModel>(packageJsonFilepath);
            RegisterPackage(packageModel);
        }

        private void RegisterPackage(PackageModel packageModel)
        {
            var targetFilePath = GetRegisteredPackageJsonFilePath(packageModel.Name);
            var targetDirName = Path.GetDirectoryName(targetFilePath);
            if (!Directory.Exists(targetDirName))
            {
                Directory.CreateDirectory(targetDirName);
            }
            JsonUtility.SaveFile(packageModel, targetFilePath);
        }


        private PackageModel GetInstalledPackageModel(string packageName)
        {
            var jsonFilePath = GetRegisteredPackageJsonFilePath(packageName);
            return JsonUtility.LoadFile<PackageModel>(jsonFilePath);
        }


        private string GetRegisteredPackageJsonFilePath(string packageName)
        {
            return PathUtility.PathCombine(PackageDBDirName, packageName, PackageArchiveUtility.PACKAGE_MODEL_FILENAME);
        }


        private string GetTemporaryDirectory()
        {            
            return PathUtility.GetTempDirectory();
        }


        private void TemporarilyUnpack(string zipFile, Action<string, PackageModel> onExtracted)
        {
            var tempDir = GetTemporaryDirectory();
            var tempFilename = "upm" + Path.GetRandomFileName();
            var extractionDirectory = Path.Combine(tempDir, tempFilename);
            Directory.CreateDirectory(extractionDirectory);
            try
            {                
                var packageModel = PackageArchiveUtility.ExtractPackageArchive(zipFile, extractionDirectory);
                if (onExtracted != null)
                {
                    onExtracted(extractionDirectory, packageModel);
                }
            }
            finally
            {
                Directory.Delete(extractionDirectory, true);
            }
        }


        private string[] ValidatePackage(string rootPath, PackageModel packageModel)
        {
            var errors = new List<string>();

            foreach (var packageFile in packageModel.Files)
            {       
                string error = null;
                string fullFilePath;
                if (rootPath != null)
                {
                    fullFilePath = Path.Combine(rootPath, packageFile.Path);
                }
                else
                {
                    fullFilePath = packageFile.Path;
                }

                if (!File.Exists(fullFilePath))
                {
                    error = string.Format("Missing '{0}'", fullFilePath);
                    //missingFiles.Add(packageFile.Path);
                }
                else
                {                    
                    var expectedChecksum = packageFile.Checksum;
                    var currentChecksum = PathUtility.CalculateChecksum(fullFilePath);
                    if (currentChecksum == expectedChecksum)
                    {
                        continue;
                        //modifiedFiles.Add(packageFile.Path);
                    }
                    error = string.Format("Modified '{0}'", fullFilePath);
                }
                errors.Add(error);
            }
            return errors.ToArray();
        }




        private void UpgradeExtractedPackageArchive(string temporaryExtractionDirectory, PackageModel newPackageModel)
        {
            var oldPackageModel = GetInstalledPackageModel(newPackageModel.Name);
            // Verify we have an existing package to upgrade
            if (oldPackageModel == null)
            {
                throw new Exception("Cannot upgrade " + newPackageModel.Name + ": Existing version is not installed");
            }

            // Validate the new package before we go and remote the existing one
            var errors = ValidatePackage(temporaryExtractionDirectory, newPackageModel);
            if (errors.Length > 0)
            {
                var errorMessage = "Cannot upgrade package, there are errors in the pacakge:";
                foreach (var error in errors)
                {
                    errorMessage += " " + error;
                }
                throw new Exception(errorMessage);
            }

            RemoveUnsafe(oldPackageModel);
            ApplyExtractedPackageArchive(temporaryExtractionDirectory, newPackageModel);
        }

        private void InstallExtractedPackageArchive(string temporaryExtractionDirectory, PackageModel packageModel)
        {            
            var existingPackageModel = GetInstalledPackageModel(packageModel.Name);
            if (existingPackageModel != null)
            {
                throw new Exception("Cannot install " + packageModel.Name + ": Existing version is already installed");
            }
            ApplyExtractedPackageArchive(temporaryExtractionDirectory, packageModel);
        }

        private string[] GetPreInstallationConflictingFiles(string packageArchiveFilePath)
        {
            var packageModel = PackageArchiveUtility.GetPackageModelFromArchive(packageArchiveFilePath);
            return GetPreInstallationConflictingFiles(packageModel);
        }

        private string[] GetPreInstallationConflictingFiles(PackageModel packageModel)
        {
            List<string> conflictingFilePaths = new List<string>();

            var existingPackageModel = GetInstalledPackageModel(packageModel.Name);

            HashSet<string> overwritableFiles = new HashSet<string>();

            if (existingPackageModel != null)
            {
                foreach (var existingPackageFile in existingPackageModel.Files)
                {
                    overwritableFiles.Add(existingPackageFile.Path);
                }
            }

            foreach (var packageFile in packageModel.Files)
            {
                if (File.Exists(packageFile.Path) && !overwritableFiles.Contains(packageFile.Path))
                {
                    conflictingFilePaths.Add(packageFile.Path);
                }
            }

            return conflictingFilePaths.ToArray();
        }

        private void ApplyExtractedPackageArchive(string temporaryExtractionDirectory, PackageModel packageModel)
        {

            if (packageModel == null)
            {
                throw new Exception("Invalid package");
            }
            var missingDeps = GetMissingDependencies(packageModel);

            // Fail if deps are missing
            if (missingDeps.Length > 0)
            {
                var error = "Cannot install package, there are missing dependencies:";
                foreach (var dep in missingDeps)
                {
                    error += " " + dep;
                }
                throw new Exception(error);
            }

            RegisterPackage(packageModel);

            for (int i = 0 ; i < packageModel.Files.Count ; i ++)
            {
                var relativePackageFile = packageModel.Files[i];
                var temporaryAbsolutePackageFile = Path.Combine(temporaryExtractionDirectory, relativePackageFile.Path);
                var targetPackageFile = relativePackageFile;
                var targetDirName = Path.GetDirectoryName(targetPackageFile.Path);
                if (!Directory.Exists(targetDirName))
                {
                    Directory.CreateDirectory(targetDirName);
                }
                File.Copy(temporaryAbsolutePackageFile, targetPackageFile.Path, false);
            }
        }

  


    }
}


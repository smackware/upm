﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
//using System.Threading.Tasks;
using UPM.Model;

namespace UPM.Package
{
    class Repository
    {
       
        private WebClient webClient = null;
        public string Name { get; set; }
        private bool enabled = true;
        public int Priority { get; set; }

        public Repository(string name, string Url, int priority)
        {
            this.Name = name;
            this.Priority = priority;
            webClient = new WebClient();
            webClient.BaseAddress = Url;
        }

        public PackageInfoModel CheckIfPresent(KeyValuePair<string, string> package)
        {
            if (enabled)
            {
                try
                {
                    webClient.QueryString.Clear();
                    webClient.QueryString.Add("version", package.Value);
                    var data = webClient.DownloadString(package.Key);
                    return JsonConvert.DeserializeObject<PackageInfoModel>(data);
                }
                catch (WebException e)
                {
                    if (e.Status == WebExceptionStatus.ProtocolError)
                    {
                        var statusCode = ((HttpWebResponse)e.Response).StatusCode;
                        if (statusCode == HttpStatusCode.NotFound)
                        {
                            //log.Warn("Package not found on repository " + Name);
                        }
                        else
                        {
                            //log.Warn("Repository " + Name + " returned code " + (int)statusCode + " " + new StreamReader(((HttpWebResponse)e.Response).GetResponseStream()).ReadToEnd());
                        }
                    }
                    else
                    {
                        //log.Warn("error accessing repository " + Name + ": " + e.Message);
                        enabled = false;
                    }

                    return null;
                }
            }
            else
                return null;
        }
    }
}

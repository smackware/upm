﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using UPM.Utility;
using UPM.Model;

namespace UPM.Package
{
    public class LocalRepositoryDB
    {

        public LocalRepositoryDB()
        {
        }

        public void UpdateRepository()
        {
            List<PackageModel> detectedPackages = new List<PackageModel>();
            List<string> zipFileNames = PathUtility.GlobFileMask("*.zip");
            for (int i = 0 ; i < zipFileNames.Count ; i++)
            {
                var zipFileName = zipFileNames[i];
                var packageModel = PackageArchiveUtility.GetPackageModelFromArchive(zipFileName);
                if (Path.GetFileName(zipFileName) != string.Format("{0}-{1}.zip", packageModel.Name, packageModel.Version))
                {
                    //log.Warn("SKIPPING: Package filename does not match naming convention: " + zipFileName);
                    continue;
                }
                detectedPackages.Add(packageModel);
            }
            RemoteRepositoryModel repo = new RemoteRepositoryModel();
            repo.Packages = detectedPackages.ToArray();
            JsonUtility.SaveFile(repo, "upmRepository.json");
        }
    }
}


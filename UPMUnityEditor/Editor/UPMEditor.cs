﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.IO.Compression;
using System.Collections;
using System.Collections.Generic;
using UPM.Package;
using UPM.Model;
using UPM.Repository;




public class UPMEditor : EditorWindow
{
	private static readonly Color _selectedPaneItemColor = new Color (0.24f, 0.37f, 0.588f);
	private static Vector2 _scrollListPosition = Vector2.zero;
	private static string _packageNameFilter = "";
    private static IRemoteRepository _remotePackageRepository;
	private static LocalPackageDB _localPackageRepository;
	private static List<PackageModel> _remotePackages;
	private static List<PackageModel> _localPackages;
    private static PackagePackerConfigModel _localPackerConfig;
	private static RemoteRepositoryConfigModel _repositoryConfig;
	private static Texture2D IconInstall = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/UPM/Editor/Icons/IconPlus.png");
	private static Texture2D IconAdd = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/UPM/Editor/Icons/IconAdd.png");
	private static Texture2D IconRemove = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/UPM/Editor/Icons/IconMinus.png");
	private static Texture2D IconFix = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/UPM/Editor/Icons/IconFix.png");

	private static string _errorConsole;

	private static UPMEditor _windowInstance;
	private static UPMEditor WindowInstance 
	{
		get
		{
			return _windowInstance;
		}
		set
		{
			if (_windowInstance != null && _windowInstance != value)
			{
				_windowInstance.Close ();
			}
			_windowInstance = value;
		}
	}

	public static void LogError(string text)
	{
		Debug.LogError(text);
		_errorConsole = text;
	}

	public static void LogInfo(string text)
	{
		Debug.Log(text);
		_errorConsole = text;
	}

	[MenuItem("UPM/Package Manager Window")]
	public static void ShowWindow()
	{
		//var editorWindow = EditorWindow.GetWindow(typeof(UPMEditor));
		var editorWindow = ScriptableObject.CreateInstance<UPMEditor>();
		RefershLocalPackageInfo ();

		RefreshInventory ();
		//editorWindow.maxSize = new Vector2(600, 400);
		//editorWindow.minSize = new Vector2(600, 400);
		_scrollListPosition = Vector2.zero;
		_errorConsole = "";
		editorWindow.position = new Rect(Screen.width / 2, Screen.height / 2, 700, 400);	
		editorWindow.ShowUtility ();
		WindowInstance = editorWindow;
	}

	private static void RefershLocalPackageInfo()
	{
		var p = new Packer ();
        _localPackerConfig = p.LoadPackerConfig ();
	}

	private static void RefreshInventory()
	{
		if (!MultipleRepositoryConfigManager.IsDefaultConfigAvailable ())
		{
			_repositoryConfig = null;
			_remotePackages = new List<PackageModel>();
			_remotePackageRepository = null;
			return;
		}
		_repositoryConfig = MultipleRepositoryConfigManager.FromDefaultConfigFile ();
        try
        {
			_remotePackageRepository = new MultipleRemoteRepositoriesDB(_repositoryConfig);
            _remotePackages = new List<PackageModel>(_remotePackageRepository.ListPackages ());
        }
        catch (Exception e)
        {
            Debug.LogError("Failed fetching packages from remote repositories: " + e.Message);
            _remotePackages = new List<PackageModel>();
        }
		_localPackageRepository = new LocalPackageDB ();
		_localPackages = new List<PackageModel>(_localPackageRepository.GetInstalledPackages());
	}

	private static bool IsInstalled(PackageModel packageModel)
	{
		foreach (var p in _localPackages)
		{
			if (p.Name == packageModel.Name && p.Version == packageModel.Version)
			{
				return true;
			}
		}
		return false;
	}

	private static bool IsAvailableForInstall(PackageModel packageModel)
	{
		foreach (var p in _remotePackages)
		{
			if (p.Name == packageModel.Name && p.Version == packageModel.Version)
			{
				return true;
			}
		}
		return false;
	}

	private int _currentPanelId;

	private SidePanelData[] _panels = new []
	{
		new SidePanelData("Manage Packages", DrawPackageSelectionPanel),
		new SidePanelData("Repositories", DrawRepositoryConfigPanel),
		new SidePanelData("Create Packages", DrawPackageCreationPanel)
	};

	private class SidePanelData
	{
		public readonly string Name;
		public readonly Action DrawCallback;

		public SidePanelData(string name, Action drawCallback)
		{
			Name = name;
			DrawCallback = drawCallback;
		}
	}

	private static bool PanelSelectionLabel(bool isOn, string title)
	{		
		GUIStyle fontGUIStyle = new GUIStyle ();
		fontGUIStyle.normal.textColor = Color.white;
		fontGUIStyle.alignment = TextAnchor.MiddleRight;
		fontGUIStyle.padding = new RectOffset (5, 5, 5, 5);
		Rect controlRect = GUILayoutUtility.GetRect (200, 50);
		int controlID = GUIUtility.GetControlID (FocusType.Passive);
		switch (Event.current.GetTypeForControl (controlID))
		{
		case EventType.Repaint:
			{				
				if (isOn)
				{
					EditorGUI.DrawRect (controlRect, _selectedPaneItemColor);
				}

				//GUIStyle.none.alignment = TextAnchor.MiddleRight;
				GUI.Label (controlRect, title, fontGUIStyle);
				//GUIStyle.none.Draw (controlRect, new GUIContent (title), controlID);

				break;
			}
		case EventType.MouseDown:
			{				
				// If the click is actually on us...
				if (controlRect.Contains (Event.current.mousePosition) 
					// ...and the click is with the left mouse button (button 0)...
					&& Event.current.button == 0)
					// ...then capture the mouse by setting the hotControl.
					GUIUtility.hotControl = controlID;
				break;
			}
		case EventType.MouseUp:
			{
				// If we were the hotControl, we aren't any more.
				if (GUIUtility.hotControl == controlID)
					GUIUtility.hotControl = 0;

				break;
			}
		}
		if (Event.current.isMouse && GUIUtility.hotControl == controlID) {			
			isOn = !isOn;
			GUI.changed = true;
			Event.current.Use ();
		}
		return isOn;
	}

	private void DrawMessageConsole()
	{
		EditorGUILayout.TextArea(_errorConsole , GUILayout.MaxHeight(75) );
	}

	private SidePanelData DrawSidePane()
	{
		EditorGUILayout.BeginVertical(GUILayout.Width (200));
		Rect controlRect = GUILayoutUtility.GetRect (200, 30);
		bool closeButton = GUI.Button (controlRect, "Close and refresh assets");
		EditorGUILayout.Space ();
		if (closeButton)
		{
			WindowInstance.Close ();
			AssetDatabase.Refresh ();
		}

		for (int panelId = 0; panelId < _panels.Length ; panelId++)
		{
			var panelData = _panels [panelId];
			var isSelected = PanelSelectionLabel (panelId == _currentPanelId, panelData.Name);
			if (isSelected)
			{
				_currentPanelId = panelId;
			}
		}
			
		EditorGUILayout.EndVertical ();
		return _panels [_currentPanelId];
	}


	private static void DrawRepositoryConfigPanelToolbar()
	{
		EditorGUILayout.BeginHorizontal();

		if (GUILayout.Button (IconAdd))
		{
			if (_repositoryConfig == null)
			{
				_repositoryConfig = new RemoteRepositoryConfigModel() { URLs = new string[0]};
			}
			List<string> urls = new List<string> (_repositoryConfig.URLs);
			urls.Add ("http://");
			_repositoryConfig.URLs = urls.ToArray ();
		}
		if (GUILayout.Button ("Save and refresh inventory"))
		{			
			MultipleRepositoryConfigManager.SaveConfigFile (MultipleRepositoryConfigManager.DEFAULT_CONFIG_FILENAME, _repositoryConfig);
			RefreshInventory ();
		}
		GUILayout.FlexibleSpace ();
		EditorGUILayout.EndHorizontal();
	}

	private static void DrawRepositoryConfigPanel()
	{				
		EditorGUILayout.BeginVertical();
		EditorGUILayout.Space();
		DrawRepositoryConfigPanelToolbar ();
		EditorGUILayout.Space();

		if (_repositoryConfig != null) 
		{
			for (int i=0; i < _repositoryConfig.URLs.Length; i++)
			{
				var url = _repositoryConfig.URLs [i];
				EditorGUILayout.BeginHorizontal ();
				Rect controlRect = GUILayoutUtility.GetRect (200, 3000, 20, 20);
				_repositoryConfig.URLs[i] = (GUI.TextField (controlRect, url));
				if (GUILayout.Button (IconRemove))
				{
					List<string> urls = new List<string> (_repositoryConfig.URLs);
					urls.RemoveAt (i);
					_repositoryConfig.URLs = urls.ToArray ();
					i--;
				}

				EditorGUILayout.EndHorizontal ();
			}
		}

		EditorGUILayout.EndVertical();
	}

	private static void DrawPackageCreationPanel()
	{
		EditorGUILayout.BeginVertical();
		EditorGUILayout.Space();
		if (_localPackerConfig != null)
		{
			foreach (var packageToPack in _localPackerConfig.Packages)
			{
				var pack = GUILayout.Button ("Pack package " + packageToPack.name + "-" + packageToPack.version);
				if (pack )
				{
					var packer = new Packer ();
					try
					{
						packer.Pack (packageToPack.name);
						LogInfo("Successfully packged " + packageToPack.name);
					}
					catch (Exception e)
					{
						LogError (e.Message);
					}
				}    
			}
		}	
		EditorGUILayout.EndVertical();
	}


	private static void DrawPackageSelectionListItem(PackageModel packageModel)
	{		
		var rowStyle = new GUIStyle ();
		rowStyle.alignment = TextAnchor.MiddleLeft;



		bool canInstall = IsAvailableForInstall(packageModel);
		bool isInstalled = IsInstalled(packageModel);
		bool canUpgrade = isInstalled && canInstall;

		EditorGUILayout.BeginHorizontal(rowStyle);
		bool installButton = false;
		bool removeButton = false;
		var style = new GUIStyle ();
		style.fixedWidth = 30;
		style.fixedHeight = 30;



		if (!isInstalled)
		{
			Rect controlRect = GUILayoutUtility.GetRect (60, 30);
			installButton = GUI.Button (controlRect, IconInstall);
			//installButton = GUILayout.Button ("+");
		}
		else if (canUpgrade)
		{
			Rect controlRect = GUILayoutUtility.GetRect (30, 30);
			installButton = GUI.Button (controlRect, IconFix);
			controlRect = GUILayoutUtility.GetRect (30, 30);
			removeButton = GUI.Button (controlRect, IconRemove);
		}
		else
		{
			Rect controlRect = GUILayoutUtility.GetRect (60, 30);
			removeButton = GUI.Button (controlRect, IconRemove);
		} 

		if (installButton)
		{
			try 
			{
				_remotePackageRepository.Install(packageModel.Name, packageModel.Version);
				LogInfo ("Installed package " + packageModel.Name);
			}
			catch (Exception e)
			{
				LogError (e.Message);
			}

			RefreshInventory();
		}
		if (removeButton)
		{
			try
			{
				_localPackageRepository.Remove(packageModel.Name);
				LogInfo ("Removed package " + packageModel.Name);
			}
			catch (Exception e)
			{
				LogError (e.Message);
			}
			RefreshInventory();
		}
		var packageNameStyle = new GUIStyle ();
		packageNameStyle.normal.textColor = Color.white;
		packageNameStyle.padding = new RectOffset (5, 5, 5, 5);
		packageNameStyle.alignment = TextAnchor.MiddleLeft;
		var labelRect = GUILayoutUtility.GetRect (30, 200, 30, 30);
		EditorGUILayout.BeginHorizontal();
		GUI.Label(labelRect, packageModel.Name, packageNameStyle);
		GUILayout.FlexibleSpace ();

		GUILayout.Label(packageModel.Version, packageNameStyle);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndHorizontal();
	}

	private static void DrawPackageSelectionFilterToolbar()
	{
		var style = new GUIStyle ();
		style.stretchWidth = true;
		EditorGUILayout.BeginHorizontal(style);
		GUILayout.Label ("Filter:");
		var rect = GUILayoutUtility.GetRect (150, 20);
		_packageNameFilter = GUI.TextField (rect, _packageNameFilter);
		GUILayout.FlexibleSpace ();
		EditorGUILayout.EndHorizontal();
	}

	private static void DrawPackageSelectionPanel ()
	{
		EditorGUILayout.Space();
		if (_remotePackages == null)
		{
			return;
		}

		EditorGUILayout.BeginVertical();
		EditorGUILayout.Space();
		DrawPackageSelectionFilterToolbar ();
		EditorGUILayout.Space();

		_scrollListPosition = EditorGUILayout.BeginScrollView(_scrollListPosition, GUILayout.Height (300));
		bool encounteredError = false;	
		for (int i = 0 ; i < _remotePackages.Count ; i++)
		{
			var packageModel = _remotePackages [i];
			var lowerPackageName = packageModel.Name.ToLower ();
			if (!string.IsNullOrEmpty (_packageNameFilter) && !lowerPackageName.Contains (_packageNameFilter.ToLower ()))
			{
				continue;
			}
			DrawPackageSelectionListItem (_remotePackages [i]);
		}

		EditorGUILayout.EndScrollView();

		EditorGUILayout.EndVertical ();
	}



	protected void OnGUI()
	{
		var rootGroupStyle = new GUIStyle();
		rootGroupStyle.padding = new RectOffset(10, 10, 10, 10);
		EditorGUILayout.BeginVertical (rootGroupStyle);
		EditorGUILayout.BeginHorizontal();
		var sidePanelData = DrawSidePane ();
		EditorGUILayout.Space();
		sidePanelData.DrawCallback ();
		EditorGUILayout.EndHorizontal();
		GUILayout.FlexibleSpace ();
		DrawMessageConsole ();
		EditorGUILayout.EndVertical ();
	}
}
